## UNIDAD I: MEDIDAS Y MATERIALES

### METROLOGÍA BÁSICA

* **Conceptos:** Metrología y Medida
* **Sistemas de medición:** Ingles y metrico
    * Conversiones entre sistemas
* **Conversiones entre sistemas**
* **Prácticas de medición con:**
    * Pie de Metro
    * Micrómetros
    * Vernie

### MATERIALES INDUSTRIALES

* **Tipos de materiales:**
    * Aceros
    * Plásticos
    * Bronces
    * Aluminios
* **Conceptos básicos de materiales:**
    * Obtención
    * Clasificación
    * Manejo de Tablas (Usos)

## UNIDAD II: EQUIPOS Y ENGRANEJES

### BUJES

* **Funcionamiento**
* **Inspección**
* **Análisis de fallas**
* **Ajustes y montaje**

### RODAMIENTOS

* **Tipos**
* **Usos**
* **Mantenimiento**
* **Detección de Fallas**
* **Manejo de Catálogos**

### CADENAS Y POLEAS

* **Tipos**
* **Usos**
* **Mantenimiento**
* **Cálculos de Transmisiones**
* **Detección de Fallas**

### BOMBAS CENTRÍFUGAS INDUSTRIALES

* **Funcionamiento**
* **Tipos**
* **Diagnóstico de fallas y su corrección:**
    * Pérdida de capacidad
    * Cavitación
    * Temperatura
    * Sobrecarga del motor
* **Inspección, ajustes y reparaciones**

### EQUIPOS ROTATIVOS/BOMBAS DESPLAZAMIENTO POSITIVO

* **Tipos de equipos rotativos:**
    * Engranes
    * Lóbulos
    * Aspas
    * Bombas rotativas
    * Bombas de vacío
    * Compresores
    * Sopladores
* **Funcionamiento**
* **Localización de problemas**
* **Soluciones y ajustes:**
    * Dependiendo del tipo de fluido
    * Dependiendo de la temperatura

### AJUSTES Y ELEMENTOS DE TRANSMISIÓN

* **Clases y tipos de ajustes:**
    * Dependiendo de la carga
    * Dependiendo de la velocidad
    * Dependiendo de la temperatura
* **Elementos de transmisión:**
    * Engranes
    * Acoplamientos
    * Impulsores
    * Poleas
    * Ventiladores
    * Transmisiones por bandas
    * Transmisiones por cadenas
    * Transmisiones por ejes
    * Transmisiones por engranajes (reductores)
* **Montaje, revisión, ajuste, fallas, lubricación y mantenimiento**

### PRINCIPIOS PRÁCTICOS DE HIDRÁULICA Y NEUMÁTICA INDUSTRIAL

* **Funcionamiento**
* **Diagnóstico de fallas y corrección de:**
    * Controles direccionales
    * Válvulas
    * Actuadores
* **Simbología**
* **Lectura e interpretación de diagramas**
* **Localización de averías en los circuitos**
* **Diagnóstico y análisis de las fallas más comunes**

### SELLOS (ESTOPEROS, RETENES Y SELLOS MECÁNICOS)

* **Aplicaciones**
* **Montaje**
* **Ajuste**
* **Elección del tipo adecuado**
* **Análisis y corrección de fallas prematuras**

## UNIDAD III: MANTENIMIENTO INDUSTRIAL

### LUBRICANTES

* **Características**
* **Tipos**
* **Usos**
* **Tablas de lubricantes**

