# Guia de estudio

* Uso del verniel [L1](https://youtu.be/CEJuZ2_kXmI)

## Fundamentos de Mecánica

<!--
Mecanica de cuerpo rigido.
1. Rodamiento o cojinetes
- Lubricacion
2. Chumasera
3. Chaveta
4. Ejes y flechas
5. Engranajes
-->
* Día 1-3: Introducción a la mecánica, principios básicos, herramientas y equipos de medición.

- ¿Que es la mecanica y cuales sus tipos?
<!--
1. ¿Que es la mecanica
2. Mecanica de cuerpo rigido, cuerpos deformables, de fluidos.
3. a) Elem. mecanicos (ejes, chumaseras, baleros, acoplamiento)
   b) Herramientas (torquimetro, verniel, micrometro)
   c) Metrologia
   d) lubricacion
-->
* Día 4-5: Mecánica de sólidos: tensiones, deformaciones, resistencia de materiales.
* Día 6-7: Mecánica de fluidos: presión, flujo, bombas, válvulas.
* Día 8-1: Mantenimiento preventivo y correctivo en sistemas mecánicos.
* Día 114: Práctica en el uso de herramientas, desmontaje y montaje de componentes mecánicos.

## Sistemas Eléctricos

**Introducción a la electricidad, leyes básicas, circuitos eléctricos**


* Día 18-19: Sistemas de potencia: motores, generadores, transformadores.

* Día 20-21: Sistemas de control: relés, contactores, PLCs.

* Día 22-24: Mantenimiento preventivo y correctivo en sistemas eléctricos.

* Día 25-28: Práctica en el uso de multímetros, osciloscopios, soldadura de componentes eléctricos.

Arranque y control de motores

## Sistemas Hidráulicos

<!-- Oleohidraulica: Hidraulica con aceite -->
**Introducción a la hidráulica, principios básicos, componentes hidráulicos.**
[Componentes oleohidraulico](https://youtube.com/playlist?list=PLYNBknF2fAh6W8tRhHO78AUgjcu2crSK2)
[Valvulas direcionales](https://youtu.be/jEO09W2PPQg)
[Circuito neumatico](https://youtu.be/Vk9JdCDFovE)7
[Simbologia](https://youtu.be/2NgStMSlCYc)
[Presostato](https://youtu.be/CenOM5S9jzA?si=mSWy6KdJYYr1n1HY)
<!--
1. Unidad Hidraulica: Bomba, motor, tanque, valvulo de alivio o de seguridad, pistones, filtros, manometro...
2. Presion. asbsuluta, manometrica, atmoferica
3. Instrumento de medicion para diagnosticar: manometro. (mejor glicerina)
4. ¿Como se elige una bomba? Mediante el flujo/min.
5. Tener en cuenta la relacion bomba/motor.
6. Como se elige un tanque

* Valvula
- Direcional o distribuidora.
- Limitadora de presion.
- Check.
- Reguladora o reductora.
- Contrabalance.
- Sobrepresion.
-->
<!-- Presurizador e hidroneumatico -->
**Sistemas hidráulicos: bombas, válvulas, cilindros, motores.**
**Sistemas neumáticos: compresores, válvulas, cilindros, motores.**
* [x] Valvula de alivio [1](https://youtu.be/gHoOcCG2j_c)
* Presostato
**Mantenimiento preventivo y correctivo en sistemas hidráulicos e neumáticos.**
**Práctica en el uso de herramientas hidráulicas, desmontaje y montaje de componentes hidráulicos.**


## Prácticas en Taller

* Día 63-64: Revisión y mantenimiento de maquinaria industrial.
* Día 65-68: Diagnóstico y reparación de fallas en sistemas mecánicos, eléctricos e hidráulicos.
* Día 69-70: Elaboración de informes técnicos y planes de mantenimiento.

