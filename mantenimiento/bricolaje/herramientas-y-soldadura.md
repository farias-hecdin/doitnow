# Maquinas y herramientas

## 1. Soldaduras

* [x] Precauciones al conetar una maquina de soldar [1Btgya]
- [ ] Soldar con soplete [1](https://youtu.be/b3WC-j3aY3Q?si=SR7w55YeX1fq1dNz)
- [ ] Soldadura por Arco Eléctrico
- [ ] Tipo de punta de destornilladores [1](https://www.casamyers.com.mx/blog/tipo-de-punta-de-destornilladores/)
- [ ] Partes de Taladro [1](https://www.homedepot.com/c/ab/how-to-use-a-drill/9ba683603be9fa5395fab9022a5fa8b)
- [ ] Tipos de brocas + consejos [1](https://youtu.be/OyHYHBYHpcg?si=-3N-6Kz5Ydq86dGA)

[1Btgya]: https://youtu.be/wfnsuVJnMkg
