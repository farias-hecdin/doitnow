<!-- toc omit heading -->
# SISTEMAS ELÉCTRICOS

> Volver al [inicio](../../README.md)

## GENERALIDADES

### 1. FUNDAMENTOS

* **Magnitudes basicas:**
    * [x] ¿Que es la intensidad, el voltaje y la resistencia eléctrica? [A1](https://youtu.be/f7p6nIKMYWo)
    * [x] ¿Qué es la ley de Ohm? [A1](https://youtu.be/Az9YgKQ_UH4)
* **Potencia eléctrica:**
    * [x] ¿Qué es la potencia eléctrica? [A1](https://youtu.be/nT1t_DbO3xU)
    * [x] ¿Qué es el factor potencia y el triangulo de potencia? [A1](https://youtu.be/XSkv07e3Olk)
    * [x] ¿Como mejorar el factor potencia (FP)? [A1](https://youtu.be/pebkVPmUeVY)
    * [x] ¿Como mejorar el FP con un banco de condesadores? [A1](https://youtu.be/TDN0AFAUk8U)
* **Analisis de circuitos:**
    * [ ] ¿Comó se análizan los circuitos en serie y en paralelo? [#1](https://tinyurl.com/t3phjje)
    * [ ] ¿Que es la ley de Kirchhoff?
* **La caida de tension:**
    * [ ] ¿Cual es su definicion y soluciones posibles? [#1](https://youtu.be/IHlVP35no70)
    * [ ] ¿Que es la resistividad electrica?
    * [ ] ¿Que son los materiales conductores y aislantes? [#1](https://youtu.be/NMD6ITCREyU)
* **Tipos de corriente:**
    * [x] La corriente alterna y la corriente continua. [#1](https://youtube.com/shorts/lMK-3iqTxcA)
    * [x] ¿Cuánta corriente eléctrica puede soportar el cuerpo humano antes de sufrir daños? [#1](https://tinyurl.com/26t67zmo) [#2](https://youtu.be/B7YdoiZmBG0)
* **Los simbolos electricos:**
    * [x] La simbologia IEC y ANSI. [#1](https://symbols.radicasoftware.com/225/iec-symbols)

### 2. INSTRUMENTOS DE MEDICION

* **El multimetro y la pinza amperimétrica:**
    * [x] ¿Qué es y cuales son sus funciones? [A1](https://youtu.be/qJ-q5CHEBvQ)
    * [x] ¿Cómo medir la resistencia y la continuidad con un multimetro? [1](https://youtu.be/o0S807UPVgA)
    * [x] ¿Cómo medir el voltaje alterno y continuo con un multimetro? [1](https://youtu.be/ikQTE3Xfy8o)
    * [ ] ¿Cómo medir la capacitancia y la inductancia con un multimetro?
    * [ ] ¿Cómo usar la función de prueba de diodos en un multímetro?
    * [ ] ¿Cómo usa la función de medición de temperatura en un multímetro?
    * [x] ¿Cómo identificar los cables de fase, neutro y tierra? [1](https://youtube.com/shorts/So_FHXZfxEY)
    * [ ] ¿Cómo identificar una fuga o derivación a tierra? [1](https://youtu.be/tNx1BDhzWfg) [2](https://youtu.be/I9k9p9PI2fg)
2. [ ] ¿Cómo se usa un osciloscopio para medir señales eléctricas?

Recursos adicionales:
- Mas material dedicados al uso del multimetro. [1](https://youtube.com/playlist?list=PLWi_4AYJf2W5YiPFHdGuyG7Yfnq7Jzn47)
<!-- [2Btgya]: https://youtu.be/X4AI-IwTM-k -->

### 3. Componentes electricos y electronicos

1. [x] El disyuntor diferencial [1](https://youtu.be/QBwllztaLps) [2](https://youtu.be/HItSO5IeWSo)
    - [x] ¿Cómo reparar un salto del disyuntor diferencial?
2. [x] El disyuntor termomagnético? [1](https://youtu.be/H2JVfRVqaeM)
    - [x] ¿Cuáles son los datos característicos de un disyuntor termomagnético? [1](https://youtu.be/wBy1vtMwpws)
    - [x] ¿Cual es la diferencia entre un diyuntor magnetotérmico y un diferencial? [1](https://youtube.com/shorts/XyQWRud5CUk?si=4iIsuoCenBML11YM)
    - [ ] ¿Cómo calcular el valor de una pastilla termoeléctrica? [1]
3. [ ] ¿Qué son los diodos rectificadores y convertidores?
4. [x] ¿Qué son los contactores? [1] [2]
    - Dimencionamiento [1](https://youtu.be/2zIUz8foT3Q)
5. [ ] Guardamotor [1](https://youtu.be/PCv8Zb23Kkw?si=kxi2QdvV_pVh6zIR)
6. [ ] Telerruptor
7. [x] Relés [1](https://youtu.be/N_PlBlEBgmI)
    - [ ] Rele termico
8. [ ] Temporazador neumatico y electronico
9. [ ] Pulsadores
10. [ ] Fusibles [1]
11. [ ] Diodo [1]
12. [ ] Capacitor o condensador [1]
    - [x] ¿Como descargar un condesador correctamente? [1](https://youtu.be/ZNpYbUOQmME)
    - [ ] Banco de condesadores
13. [ ] Potenciómetros
14. [ ] Transformador electronico
15. [x] Procedimiento para localizar un corto-circuito en un sistema eléctrico.

[3IgXiB]: https://youtu.be/I4qAybp_nJk
[3VNWWj]: https://youtu.be/9T8mg18aIKg
[3VNWWj1]: https://youtu.be/Paa63oF68tM
[3UbCFv]: https://youtu.be/EfVae8TUw9s
[3eNUgz]: https://youtu.be/zt81CaVuV74
[3ydyIR]: https://m.youtube.com/watch?v=K5U5_f1zRsc
[diodo]: https://youtu.be/Y5VQ0K__YQ4?si=WHubFbxUy72M09Pf

### 4. Instalaciones eléctricas

1. [ ] Armar un tablero eléctrico de 110v
    - [ ] Armar un tablero eléctrico de 220v
    - [x] Girnalda [1](https://youtu.be/mQ3I4zAHbpw)
    - [ ] ¿Es una barra para neutros o una barra para tierra fisica?
    - [x] ¿Qué es la carga instalada, demanda y de consumo? [2odDNo]
2. Instalaciones electricas
    - [ ] Instalación de múltiples tomas de corriente
    - [ ] Instalación de interruptores
    - [ ] Instalación de iluminación
    - [ ] Instalación múltiples tomas de corriente [1](https://youtu.be/HGZU8Pczguo?si=PP1G0pfK1mU7BsGU)
    - [ ] Instalación interruptores
1. [x] Empalmar dos cables [D09a]
    - [x] Empalmar tres cables o más [D10a] [2](https://youtu.be/KHsZcBByFtE)
1. [x] Cálculo de la sección de cable necesaria [D11a] [D11b]
1. [x] ¿Cómo determinar el calibre de cable necesario para un número de bombillos? [4XYoMS]
1. [x] Uso de conectores y terminales eléctricos
1. [ ] Designacion tecnica de un cable [uXWvD]

[D09a]: https://youtube.com/shorts/uwa2c0CjXtU?si=qufu_ycWWf1rUK6d
[D10a]: https://youtu.be/IOled5OVgiM?si=2ukW6vPaE_mh-eVd
[2odDNo]: https://youtu.be/yQug26ieo98
[X11a]: http://youtube.com/shorts/xlHr1AwPq6U
[D11a]: https://www.ferremoki.com/post/como-saber-que-cable-usar-para-mi-instalaci%C3%B3n-tabla-de-calibres-de-cables-y-amperaje-mokitips
[D11b]: https://youtu.be/y5Mz7MAK6Qo
[uXWvD]: https://estructurasdecargaelectrica.blogspot.com/2017/10/conductores-o-cables-electricos.html
[4XYoMS]: https://youtu.be/lgQpYoJz3Ys

### 5. Motores electricos

* [ ] Generalidades
* [ ] Diagrama de controles electricos [1](https://youtu.be/48bit18JYvQ)
* [ ] Instalación de motores eléctricos
* [ ] Tipos de motores eléctricos
* [ ] Metodos de arranque
* [ ] Inversion de giro [E05a](https://youtu.be/tvUA45B2YkQ)
* [ ] Instalacion de contactores
* [ ] Relevador de sobrecarga
* [ ] Arranque directo motor trifasico
* [ ] Partes de un motor electrico [1](https://tractian.com/es/blog/como-funcionan-los-motores-electricos)
* [x] Como conectar un motor trifasico de 6 [1](https://youtu.be/g4kOd_xWjw8)
* [x] Como conectar un motor trifasico de 9 terminales. [1](https://youtu.be/48toyR_C43w) [2](https://youtu.be/0zG1Lt11uTA)

[E01a]: https://youtu.be/8GbEupPFVEM?si=FpslRUz3rTwuyOta
[E09a]: https://youtu.be/gGWo0VvMedg
[E05a]: [E05b]: https://youtu.be/9iYTU5tSqyA

## Programmer Logic Controller

### 6. PLC

* Fundamentos [6rQVds](http://youtu.be/1grnjc5QVds)

<!-- Puerta logica [1](https://youtu.be/3t3Pgve1mvw) -->
