# Electronica y equipos

## A. Electronica

- [ ] ¿Como soldar con un cautin? [A01a]

[A01a]: https://m.youtube.com/watch?v=NC2Vkf4pBKk

## Equipos

### B. Refrigeracion

- [ ] Desarmar un aire-acondicionado inverter
- [ ] ¿Como revisar un capacitador?
- [ ] Sistema de Refrigeracion AC marca York

[B01a]: https://youtu.be/z3_t3aPMh1g
[B02a]: https://youtu.be/gJcxs04YGqA
