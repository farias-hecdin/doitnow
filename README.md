# Base de conocimiento
Este espacio tiene como objetivo presentar de manera clara y concisa la información más relevante sobre un tema específico, con la finalidad de ayudarlo a comprender y aprender el tema para que pueda ser puesto en práctica.

* **Mantenimiento:**
    1. Bricolaje <!-- RED -->
        * Albañileria y Fontaneria
        * Herramientas y Soldadura
    2. Electricidad y electronica <!-- BLUE -->
        * [Instalaciones electricas](./mantenimiento/electricidad-y-electronica/instalaciones-electricas.md)
        * Electronica y equipos
    3. Mecanica y materiales <!-- PURPLE -->
        * Mecanica automotriz
        * Corrosion
    4. Hidraulica y neumatica
        * Hidraulica
* **Computación:** <!-- GREEN -->
    1. Bash
    2. Java
    3. Javascript
    4. Php
    5. Sql
    6. Lua
    7. Golang
    8. Zig
* **Administración:**
    1. Contabilidad
* **Idiomas:** <!-- GRAY -->
    1. Ingles
    2. Portugues
