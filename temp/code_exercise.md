Crea un sistema de gestión de biblioteca que permita a los usuarios agregar, eliminar y buscar libros. El sistema debe tener las siguientes características:


# Puedes darme un enuciado de un ejercicio para practicar javascript. El ejercicio debe aplicar todos los fundamentos del lenguaje, como por ejemplo:

* ... (spread)
* .at()
* .every()
* .filter()
* .find()
* .forEach()
* .includes()
* .join()
* .length
* .map()
* .push()
* .reduce()
* .some()
* .splice()
* ?. (optional chaining)
* Arrays of objects
* [] (get item)
* destructuring
* Classes
* => (arrow functions)
* ?. (optional chaining)
* Asynchronous callbacks
* Callback pattern
* Closures
* Generator functions
* Hoisting
* Lexical scope
* Passing functions
* basic functions
* default parameters
* implicit return
* yield
* yield*
* Currying
* Deep equal
* ECMAScript
* Event loop
* IIFE
* Immutability
* Intro to Regular Expressions
* Intro to functional programming
* Map
* Primitive types
* Set
* Strict Mode
* Window
* this
* typeof operator
* ES Modules
* Importing from libraries
* Module bundlers
* Namespace import
* Package managers
* Parcel
* Vite
* Webpack
* default export
* import
* import() (dynamic imports)
* named export
* npm
* package.json
* script type="module"
* yarn
* .toString()
* Division remainder (%)
* NaN
* Number.parseInt()
* numeric separator (_)
* ... (spread)
* ?. (optional chaining)
* ?? (nullish coalescing)
* Advanced control flow
* Object.entries()
* Object.keys()
* Object.values()
* destructuring
* dynamic properties
* in (operator)
* object shorthand
* property access
* .catch()
* .finally()
* .then()
* Promise.all()
* Promise.any()
* Promise.reject()
* Promise.resolve()
* Promises
* Top-level await
* async function
* async/await
* await keyword
* new Promise()
* states
* .at()
* .endsWith()
* .includes()
* .length
* .replace()
* .replaceAll()
* .split()
* .startsWith()
* .substring()
* .toLowerCase()
* .toUpperCase()
* .trim()
* Concatenation
* Template strings
* [] (character access)
* Exceptions
* Semi-colons
* for (loops)
* for...in
* for...of
* if
* try...catch
* try...catch...finally
* ?? (nullish coalescing)
* Hoisting
* Temporal Dead Zone
* const
* let
* var (legacy)
* DELETE
* Error handling
* Fetch Wrapper
* GET
* JSON.parse()
* JSON.stringify()
* POST
* PUT
* Response status codes
* Response.json()
