# Fundamentos 1

> [Regresar](../zona-de-inicio.md)

# Hello world

```java
/* El nombre de la primera "class" debe coincidir con el nombre del archivo */
/* Un programa en Java debe iniciar con el metodo "main()" (1) */

class Mensaje {
    public static void main(String[] args) { // (1)
        System.out.println("Hello world");
    }
}
```
