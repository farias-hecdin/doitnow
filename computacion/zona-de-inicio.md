# Computación

Volver al [inicio](../README.md)

1. [Bash](./bash/_inicio.md)
2. [Javascript](./javascript/_inicio.md)
    * [React](./javascript-react/_inicio.md)
    * Astro
3. Java
    * Spring
4. Php
    * Wordpress
    * Laravel?
5. Sql
    * Postgres?
    * MariaDB?
6. [Lua](./lua/fundamentos-1.md)
    * Love2D
7. Golang
    * Echo
8. Zsh
