# Fundamentos 1

> [Regresar](../zona-de-inicio.md)

## Comentarios

```lua
-- Este es un comentario de una sola linea.

--[[ Este es un comentario de multiples lineas
   con una etiqueta de apertura y una
   de cierre. ]]
```

## Hello world

```lua
print('Hello world')
```

## Variables y tipos de variables

```lua
-- Permite crear variables globales.
nombre = "Diego"

-- local - Permite crear variables locales.
local apellido = "Diaz"

--[[ NOTA: Las variables solo pueden contener letra, numeros, `$` o `_`. No pueden
   contener espacios o palabras reservadas. ]]
```

### Formas de declarar una o mas variable

```lua
-- Forma No.1
local año = 32
local nombre, apellido = "Juan", "Perez"
```

## Interacciones

```lua
 -- Muestra un mensaje en pantalla
print("Hello world!")
```

## Tipos de datos

### Primitivos
