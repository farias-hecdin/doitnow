---

# Javascript, Temporizadores

> Volver a: [[area_informatica]]

```toc
```

<hr id="line">

## setTimeout
Establece un temporizador que ejecuta una porción de código después de un tiempo establecido.

```Js
let mensaje = 'Hello world'
let tiempo = 2000 // En milisegundo == 2 seg.

setTimeout(() =>{
  console.log(mensaje)
}, tiempo)

```
