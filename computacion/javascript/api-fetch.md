---

# Javascript, API fetch

> Volver a: [[area_informatica]]

```toc
```

<hr id="line">

## Generalidades

```js
(() => {
  const $fetch = document.getElementById('fetch');
  const $fragment = document.createDocumentFragment();

   fetch('https://jsonplaceholder.typicode.com/users')
  .then(res => {
    // Validando la peticion.
    return (res.ok ? res.json() : Promise.reject(res));
  })
  .then(json => {
    // Ejecutando la logica.
    json.forEach(elem => {
      const $li = document.createElement('li');

     $li.innerHTML = `${elem.name} --- ${elem.phone}`;
     $fragment.appendChild($li);
      $fetch.appendChild($fragment);
    })
  })
  .catch(error => {
    // Capturando algun error.
   let mensaje = error.statusText || ' --- Ocurrio un error';

    $fetch.innerHTML = error.status + mensaje;
  })
  // Opcional.
 .finally(() => {
       console.warn('Ejecutando: .finally()')
  })
})()

```
