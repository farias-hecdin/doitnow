# Javascript, Conversiones

> Volver a: [inicio](../area-informatica.md)

## Conversiones explicitas

1. A número: `Number(..)`
2. A texto: `String(..)`
3. A booleano: `Boolean(..)`
4. Etc...

```javascript
// Declaración de variables
let num1 = 10;
let num2 = "5";

// Conversión explícita
num2 = parseInt(num2);

// Operaciones aritméticas con variables convertidas
let suma = num1 + num2;

console.log(suma, typeof(suma)) // Number
```

## Conversiones implicita

```javascript
// Declaración de variables
let num1 = 10;
let num2 = "5";

// Conversión implícita
let suma = num1 + num2;

console.log(suma, typeof(suma)) // Number
```
