# Fundamentos 1

> [Regresar](../zona-de-inicio.md)

- [Fundamentos 1](#fundamentos-1)
  - [Comentarios](#comentarios)
  - [Use strict](#use-strict)
  - [Variables y tipos de variables](#variables-y-tipos-de-variables)
    - [Formas de declarar una o mas variable](#formas-de-declarar-una-o-mas-variable)
  - [Interacciones](#interacciones)
  - [Ordenamiento de código](#ordenamiento-de-cdigo)
  - [Tipos de datos](#tipos-de-datos)
    - [Primitivos](#primitivos)
    - [Compuestos](#compuestos)
- [typeof](#typeof)
  - [Condicionales](#condicionales)
    - [If](#if)
    - [If / else](#if--else)
    - [If / else if / else](#if--else-if--else)
    - [Ternario](#ternario)
    - [Switch](#switch)

## Comentarios

```javascript
// Este es un comentario de una sola linea.

/* Este es un comentario de multiples lineas
   con una etiqueta de apertura y una
   de cierre. */
```

## Use strict

```javascript
/* Es una keyword que indica que el código debe seguir un conjunto más estricto
   de reglas para evitar errores comunes durante el desarrollo. */

"use strict";

function suma(a, b) {
   if (typeof a !== "number" || typeof b !== "number") {
        throw new Error("Sólo se pueden sumar números");
    }

    return a + b;
}

console.log(suma(2, 3)); // Salida: 5
console.log(suma("hola", 3)); // Salida: Error, Sólo se pueden sumar números
```

## Variables y tipos de variables

```javascript
// let - Permite crear variables locales.
let nombre = 'Juan';

// const - Permite crear variables cuyo valor no varía.
const PI = 3.14;

// var - Permite crear variables globales.
var esMayorDeEdad = true;

/* NOTA: Las variables solo pueden contener letra, numeros, `$` o `_`. No pueden
   contener espacios o palabras reservadas. */
```

### Formas de declarar una o mas variable

```javascript
// Forma No.1
let edad = 32;
let nombre = "Juan", apellido = "Perez";
```

## Interacciones

```javascript
// Muestra el resultado en la consola del interprete.
console.log('Hola Mundo');

// Muestra un mensaje en pantalla
alert('Hola Mundo');

// Muestra un mensaje en pantalla y solicitar un dato al usuario.
const nombre = prompt('Ingrese su nombre', 'Ninguno');

// Muestra un mensaje en pantalla seguido de dos opciones (aceptar y cancelar)
const repuesta = confirm('Estás seguro?');
```

## Ordenamiento de código

```javascript
/* Para optimizar la ejecucion de un script se recomienda seguir la siguiente
   convencion */

// 1. Importación de datos
// 2. Declaración de variables
// 3. Funciones
// 4. Lógica de la aplicación

/* Ejemplo */

import { modulo } from "./un-archivo.js";

const PI = 3.145;
let valor = 25

function calcularAreaDelcirculo(radio) {
    let res = PI * (radio * radio)
    return res
}

calcularAreaDelcirculo(valor);
```

## Tipos de datos

### Primitivos

```javascript
/* String */
let nombre = "Diego";

/* Number */
let edad = 34; // Int
let PI = 3.145; // Float
let poblacionMundial = 8085616922n; // BigInt

/* Boolean */
let eresMayorDeEdad = true;

/* Symbol */
let planeta = Symbol("Tierra");

/* Undefined y Null */
let pais; // undefined
let mascota = null; // null
```

### Compuestos

```javascript
/* Object */
let persona = { nombre: 'Juan', edad: 25 };
// Array
let arreglo = [1, 2, 3];

/* Function */
function sumarDosNumeros(a, b) {
    return a + b
}
// Arrow function
const restarDosNumeros = (a, b) => a - b


/* Class */
class Rectangulo {
  constructor(ancho, alto) {
    this.ancho = ancho;
    this.alto = alto;
  }

  area() {
    return this.ancho * this.alto;
  }
}
```

### typeof

```javascript
/* typeof - Con esta expresion podemos descubrir ¿Qué tipo de dato es? un dato
   asignado. */

let edad = 34;
console.log(typeof edad) // Salida: number
```

## Condicionales

### If

```javascript
/* if... - Esta expresion sigue el patron: Si la condición se cumple, ejecuta
   la acción indicada; si no se cumple, pasa de el. */

const numero = prompt("Ingresa un numero: ", 1);

if (numero > 0) {
  console.log("El número es positivo.");
}
```

### If / else

```javascript
/* if.. else.. - Esta expresion sigue el patron: Si la condición se cumple,
   ejecuta la accion indicada. Sino se cumple, ejecuta esta otra. */

const numero = prompt("Ingresa un numero: ", 1);

if (numero > 0) {
  console.log("El número es positivo.");
} else {
  console.log("El número es negativo.");
}
```

### If / else if / else

```javascript
const numero = prompt("Ingresa un numero: ", 1);

if (numero > 0) {
  console.log("El número es positivo.");
} else if (numero == 0) {
  console.log("El número es 0.");
} else {
  console.log("El número es negativo.");
}
```

### Ternario

```javascript
/* ? - Es una expresión que simplifica la forma de escribir condicionales
   "if.. else.." */

const numero = prompt("Ingresa un numero: ", 1);

let mensaje = (numero > 0)
    ? "El número es positivo."
    : (numero == 0)
        ? "El número es 0."
        : "El número es negativo.";

console.log(mensaje);
```

### Switch

```javascript
/* Permite evaluar varias condiciones o expresiones y ejecutar un bloque de
   código específico en función a las coincidencias encontradas. */

const ptos = +prompt("Ingresa la calificacion del estudiante del 0 al 100", 30);

switch (true) {
  case ptos >= 75:
    console.log("A");
    break;

  case ptos >= 50:
    console.log("B");
    break;

  case ptos > 25:
    console.log("C");
    break;

  case ptos > 0:
    console.log("D");
    break;

  default:
    console.log("F");
}
```
