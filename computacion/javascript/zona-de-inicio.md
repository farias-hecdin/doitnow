# Javascript

> Volver al [Inicio](../_inicio.md)

## Fundamentos 1

[Ir](./_fundamentos-1.md)

* Comentarios
* Use strict
* Variables y tipos de variables
* Formas de declarar una o mas variable
* Interacciones
* Ordenamiento de código
* Tipos de datos
* Primitivos
* Compuestos
* typeof
* Condicionales
* If
* If / else
* If / else if / else
* Ternario
* Switch

## Fundamentos 2

