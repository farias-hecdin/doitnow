---

# Javascript, Keyword (map) y (set)

> Volver a: [[area_informatica]]

```toc

```

<hr id="line_H">

## Map

Es otra manera de almacenar una coleccion de datos, al igual que `Object` y `Array`, pero con caracteristicas adicionales.

_Sintaxis_

```js
let sujeto = new Map(); // Creando un «Map».

console.log(sujeto); // Llamando al «Map».
```

### Lista de metodos

- `new Map()` – Crear una coleccion.
- `map.set(«Clave», «Valor»)` – Añadir un elemento.
- `map.get(«Clave»)` – Accede al valor de la clave.
- `map.has(«Clave»)` – Devuelve `true` si la clave existe.
- `map.delete(«Clave»)` – Elimina el valor de una clave.
- `map.clear()` – Elimina todo del map.
- `map.size` – Muestra la cantidad de elementos.

```javascript
let armario = new Map(); // Creando un «Map».
console.log(armario);

// |- Añadiendo elementos al nuevo objeto.
armario.set("repisa", { caja: 1, cosa: "tornillos"} );
armario.set("cajon", "llaves")

console.log(armario.get("cajon"));
```

### Recorriendo sobre Map

Para iterar sobre un mapa existen 3 metodos:

- `map.keys()`
- `map.values()`
- `map.entries()`

## Set
