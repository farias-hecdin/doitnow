---

# Javascript, Keyword (this)

> Volver a: [[area_informatica]]

```toc

```

<hr id="line_H">

## "this" en Javascript

`this` es una keyword con un funcionamiento muy caracteristico que merece la pena prestar mas atencion:

### En un contexto global

```js
// [01]

let a = this;
// cuando «this» esta solo se vinvula al objeto global llamado «window».
console.log(a === window); // true

this.algo = "gato";
console.log(window.algo);
```

### Dentro de una Funcion

```js
// [02]

function saludo() {
  // Pese a estar dentro de una funcion.
  // «this» se vincula a «window».
  console.log(this);
}

greet();
```

### En un Constructor

Caso **01**

```js
// [03]

function Persona() {
  this.nombre = "Jack";
  console.log(this);
}

let sujeto1 = new Persona();

// Aca «this» hace referencia a "sujeto1".
console.log(sujeto1.nombre);
```

Caso **02**

```js
// [03]
let Animal = function (a_nombre, a_color, a_patas) {
  this.nombre = a_nombre;
  this.color = a_color;
  this.patas = a_patas;
  this.apelativo = this.nombre + " " + this.color;
  this.extremidades = () => this.patas + " " + this.color;
  this.descripcion = function () {
    return "Un " + this.nombre + " de color " + this.color;
  };
};

let miAnimal = new Animal("Gato", "Blanco", 4);

// Aca «this» hace referencia a los objetos.
console.log(miAnimal.apelativo);
console.log(miAnimal.extremidades());
console.log(miAnimal.descripcion());
```

### Dentro de un Metodo

```js
// [04]

const persona = {
  nombre: "Jack",
  edad: 25,
  saludo: function () {
    console.log(this);
    console.log(this.name);
  },
};

// Aca «this» hace referencia a "persona".
persona.saludo();
```

### En una Funcion Interna

```js
// [05]

const persona = {
  nombre: "Jack",
  edad: 25,
  saludo: function () {
    // Al guardar «this» podemos refereciar al objeto desde los anidamientos.
    // let self = this;
    console.log(this);
    console.log(this.edad);

    function internaFuncion_1() {
      console.log(this); // ..(self) – referencia al Objeto.
      console.log(this.edad);

      function internaFuncion_2() {
        console.log(self); // referencia al Objeto.
        console.log(self.edad);
      }

      internaFuncion_2();
    }

    internaFuncion_1(); // window, undefined
  },
};

persona.saludo();
```

### Dentro de una Funcion Flecha

Caso **01**

```js
// [06] == [02]

const saludo = () => {
  console.log(this);
};

saludo(); // window
```

Caso **02**

```js
// [07] == [04] !== [05]

const saludo = {
  nombre: "Jack",
  diHola: function () {
    let hola = () => {
      console.log(this);
      console.log(this.nombre);
    };

    hola(); // saludo{...}, "Jack"
  },
};

// Aca «this» hacer referencia a "saludo".
saludo.diHola();
```

## call(), apply(), bind()

Estos metodos permite explicitamente a que va hacer referencia `this`.

- `call()` – Nos permite definir a que va a hacer referencia `this` –en su primer parametro– y lo siguiente los parametros que recibe la funcion.
- `apply()` – Es igual al anterior, pero se diferencia a que nos permite pasar un _Array_ como parametro de la funcion.
- `bind()` – Nos permitir crear una copia de una funcion, asignar un valor a `this` y añadir los respetivos parametros de la funcion.

```js
const hablar = function (p1, p2, p3) {
  console.log(`Hola, soy ${this.nombre} y se programar en: ${p1}, ${p2}, ${p3}`);
};

const sujeto1 = {
  nombre: "David",
  edad: 23,
};

const sujeto2 = {
  nombre: "Roberto",
  edad: 42,
};

const lenguajes = ["Javascript", "Python", "C++"];

// call(...)
hablar.call(sujeto2, lenguajes[0], lenguajes[1], lenguajes[2]);

// apply(...)
hablar.apply(sujeto1, lenguajes);

// bind(...)
const hablarSujeto = hablar.bind(sujeto1, lenguajes[0], lenguajes[1], lenguajes[2]);

hablarSujeto();
```
