
// Operadores
let suma = 1 + 2;
let resta = 10 - 5;
let multiplicacion = 5 * 2;
let division = 10 / 2;
let modulo = 10 % 3;
let incremento = 1++;
let decremento = 1--;
let asignacion = 1 = 2;
let comparacion = 1 == 2;
let comparacionEstricta = 1 === '1';
let negacion = !true;
let and = true && false;
let or = true || false;

// Conversiones
let numero = Number('123');
let texto = String(123);
let booleano = Boolean(1);

// Condicionales y bucles
if (numero > 10) {
  console.log('El número es mayor a 10');
} else if (numero < 10) {
  console.log('El número es menor a 10');
} else {
  console.log('El número es igual a 10');
}

for (let i = 0; i < 10; i++) {
  console.log(i);
}

let i = 0;
while (i < 10) {
  console.log(i);
  i++;
}

// Funciones
function sumar(a, b) {
  return a + b;
}

let resultado = sumar(1, 2);
console.log(resultado);

// Objetos
let persona = {
  nombre: 'Juan',
  edad: 25,
  direccion: {
    calle: 'Calle Falsa 123',
    ciudad: 'Ciudad Falsa',
  },
  saludar: function () {
    console.log('Hola, soy ' + this.nombre);
  },
};

console.log(persona.nombre);
console.log(persona.direccion.ciudad);
persona.saludar();

// Parámetro REST y operador SPREAD
function sumarNumeros(num1, num2, ...numeros) {
  let suma = num1 + num2;
  for (let i = 0; i < numeros.length; i++) {
    suma += numeros[i];
  }
  return suma;
}

let numeros = [1, 2, 3, 4, 5];
let resultado = sumarNumeros(1, 2, ...numeros);
console.log(resultado);

// This / bind, call
let persona = {
  nombre: 'Juan',
  saludar: function () {
    console.log('Hola, soy ' + this.nombre);
  },
};

persona.saludar();

let saludar = persona.saludar;
saludar(); // Este código no funciona porque this se refiere al objeto global (window en el navegador)

let saludar2 = persona.saludar.bind(persona);
saludar2(); // Este código funciona porque usamos bind para fijar el valor de this

// Map y Set
let mapa = new Map();
mapa.set('clave1', 'valor1');
mapa.set('clave2', 'valor2');
console.log(mapa.get('clave1'));

let conjunto = new Set();
conjunto.add(1);
conjunto.add(2);
conjunto.add(3);
console.log(conjunto.has(1));

// Iterando sobre un Map y un Set

for (let [clave, valor] of mapa.entries()) {
  console.log(clave, valor);
}

for (let valor of conjunto.values()) {
console.log(valor);
}

// This / bind, call (continuación)

// Usando call para pasar argumentos a una función
let persona = {
  nombre: 'Juan'
};

function saludar(saludo, emoji) {
  console.log(saludo + ', ' + this.nombre + ' ' + emoji);
}

saludar.call(persona, 'Hola', '😀');

// Usando apply para pasar argumentos a una función como un arreglo
let persona = {
  nombre: 'Juan'
};

function saludar(saludo, emoji) {
  console.log(saludo + ', ' + this.nombre + ' ' + emoji);
}

saludar.apply(persona, ['Hola', '😀']);

// Parámetro REST y operador SPREAD (continuación)

// Usando el operador SPREAD para concatenar arreglos
let arreglo1 = [1, 2, 3];
let arreglo2 = [4, 5, 6];
let arreglo3 = [...arreglo1, ...arreglo2];
console.log(arreglo3);

// Usando el operador SPREAD para copiar un objeto
let objeto1 = { propiedad1: 'valor1', propiedad2: 'valor2' };
let objeto2 = { ...objeto1, propiedad3: 'valor3' };
console.log(objeto2);

// Objetos (continuación)

// Creando un objeto con una función constructora
function Persona(nombre, edad) {
  this.nombre = nombre;
  this.edad = edad;
}

let juan = new Persona('Juan', 30);
console.log(juan.nombre);
console.log(juan.edad);

// Agregando métodos a un prototipo
Persona.prototype.saludar = function () {
  console.log('Hola, soy ' + this.nombre);
};

juan.saludar();

// Herencia de objetos
function Empleado(nombre, edad, puesto) {
  this.nombre = nombre;
  this.edad = edad;
  this.puesto = puesto;
}

Empleado.prototype = Object.create(Persona.prototype);
Empleado.prototype.constructor = Empleado;

let pedro = new Empleado('Pedro', 25, 'Desarrollador');
console.log(pedro.nombre);
console.log(pedro.edad);
console.log(pedro.puesto);
pedro.saludar();

// Map y Set (continuación)

// Eliminando elementos de un Map y un Set
mapa.delete('clave1');
conjunto.delete(1);

// Comprobando si un elemento existe en un Map y un Set
console.log(mapa.has('clave1'));
console.log(conjunto.has(1));

// Iterando sobre los valores de un Map y un Set
for (let valor of mapa.values()) {
  console.log(valor);
}

for (let valor of conjunto.values()) {
  console.log(valor);
}

// Funciones (continuación)

// Definición de funciones anónimas y autoinvocadas
let suma = function (a, b) {
  return a + b;
}();

console.log(suma);

// Usando funciones como argumentos y valores de retorno
function sumar(a, b) {
  return a + b;
}

function aplicarFuncion(a, b, funcion) {
  return funcion(a, b);
}

console.log(aplicarFuncion(1, 2, sumar));

// Closures
function contador() {
  let cont = 0;
  return function () {
    cont++;
    console.log(cont);
  }
}

let incrementar = contador();
incrementar();
incrementar();
incrementar();

// Generadores
function* numeros() {
  let i = 0;
  while (true) {
    yield i;
    i++;
  }
}

let gen = numeros();
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);

// Promesas
let promesa = new Promise(function (resolve, reject) {
  setTimeout(function () {
    resolve('Se ejecutó la promesa');
  }, 2000);
});

promesa.then(function (valor) {
  console.log(valor);
});

// Async/Await
async function esperar() {
  let resultado = await fetch('https://api.github.com/users/github');
  let datos = await resultado.json();
  console.log(datos);
}

esperar();

// Módulos
import \* as miModulo from './mi-modulo.js';
console.log(miModulo.nombre);
console.log(miModulo.sumar(1, 2));

// Exportando módulos
export let nombre = 'Juan';
export function sumar(a, b) {
  return a + b;
}

// Exportando módulos (forma alternativa)
let nombre = 'Juan';
function sumar(a, b) {
  return a + b;
}

export { nombre, sumar };

// Exportando módulos por defecto
export default function restar(a, b) {
  return a - b;
}

// Importando módulos por defecto
import resta from './mi-modulo.js';
console.log(resta(5, 3));

// Manipulación del DOM
let boton = document.getElementById('mi-boton');
boton.addEventListener('click', function () {
  console.log('Se hizo clic en el botón');
});

let parrafo = document.createElement('p');
parrafo.textContent = 'Este es un nuevo párrafo';
document.body.appendChild(parrafo);

// Eventos
window.addEventListener('load', function () {
  console.log('La página se ha cargado');
});

window.addEventListener('resize', function () {
  console.log('Se ha cambiado el tamaño de la ventana');
});

window.addEventListener('scroll', function () {
  console.log('Se ha hecho scroll en la página');
});

// LocalStorage
localStorage.setItem('nombre', 'Juan');
let nombre = localStorage.getItem('nombre');
console.log(nombre);

localStorage.removeItem('nombre');

// Sesión Storage
sessionStorage.setItem('nombre', 'Juan');
let nombre = sessionStorage.getItem('nombre');
console.log(nombre);

sessionStorage.removeItem('nombre');

// Cookies
document.cookie = 'nombre=Juan; expires=Thu, 31 Dec 2022 23:59:59 UTC; path=/';
let nombre = document.cookie;
console.log(nombre);

// Geolocalización
navigator.geolocation.getCurrentPosition(function (posicion) {
  console.log(posicion.coords.latitude);
  console.log(posicion.coords.longitude);
});

// Detección de dispositivos móviles
if (navigator.userAgent.match(/Mobi/)) {
  console.log('Este dispositivo es móvil');
} else {
  console.log('Este dispositivo no es móvil');
}

// Detección de navegadores
if (navigator.userAgent.match(/Chrome/)) {
  console.log('Este navegador es Chrome');
}
