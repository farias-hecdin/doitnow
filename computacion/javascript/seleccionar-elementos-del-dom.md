---

# Javascript, Seleccionar elementos del DOM

> Volver a: [[area_informatica]]

```toc
```

<hr id="line">

## X.getElementById(..)

Captura elementos HTML a traves del Id.

```html:index.html
<div id="AQUI"> </div>
```

```js:main.js
const $div = document.getElementById("AQUI");
console.dir($div)
```

## X.querySelector(..)
