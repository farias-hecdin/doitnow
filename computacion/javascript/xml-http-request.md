---

# Javascript, XML Http Request (xhr)

> Volver a: [[area_informatica]]

```toc
```

<hr id="line">

## Generalidades

```Js
(() => {
  // [1] – Crear la instancia HTTP.
  const ajax = new XMLHttpRequest();
  const $ajax = document.getElementById('ajax'),
        $fragment = document.createDocumentFragment(); // Crear los elementos HTML y hace una sola insersion al DOM.
  
  // [2] – Añadir los eventos necesarios.
  ajax.addEventListener('readystatechange', (e) => {
    // Logica de la aplicacion.
    if (ajax.readyState !== 4) return;

    if (ajax.status >= 200 && ajax.status <= 299) {
      let dataJson = JSON.parse(ajax.responseText);
      dataJson.forEach(elem => {
        const $li = document.createElement('li');
        $li.innerHTML = `${elem.name} --- ${elem.phone}`;
        $fragment.appendChild($li);
        $ajax.appendChild($fragment)
      })
    } else {
      let message = ajax.statusText || " --- ¡Oh! Error";
      $ajax.innerHTML = ajax.status + message;
    }
  })
  
  // [3] – Abrir la peticion y establecer el metodo a ejecutar.
  ajax.open('GET', 'https://jsonplaceholder.typicode.com/users');
  // [4] – Enviar la peticion.
  ajax.send();
})()
```
