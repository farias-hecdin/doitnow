# Javascript, Funciones FIXME

> Volver a: [inicio](../area-informatica.md)

## Funcion declarada

Las funciones son bloques de código que contienen instrucciones para realizar una determinada tarea, su sintaxis es la siguiente:

```javascript
function «[1] Nombre»(«[2] Parámetros, separados, por, comas») {
	«[3] Bloque de código»
}

«[1] Nombre»(«[2] Parámetros»);
```

Caso **01**

```javascript
let nombre = "Juan"; // Variable, alcance global

function showMensaje() {
  let mensaje = "Hola", // Variable, alcance local
    mostrar = mensaje + ", " + nombre;

  console.log(mostrar);
}

showMensaje();
```

Caso **02** – Incluyendo parámetros predeterminados.

```javascript
function showMensaje() {
  let mensaje = "«dato desconocido»";

  return mensaje;
}

function showDatos(nombre = showMensaje(), apellido = showMensaje()) {
  // Param. por defecto
  console.log(`
		Nombre: ${nombre}
		Apellido: ${apellido}
	`);
}

showDatos("Alex");
```

### Características

- **Función recursiva** – Es una función que se llama a sí misma durante su ejecución.

  ```javascript
  function contador(numero) {
    // [1]
    console.log(numero);
    const newNumero = numero - 1;

    if (numero > 0) {
      contador(newNumero); // [1] Reutilizando la funcion inicial
    } else {
      console.log("¡Listo!");
    }
  }

  contador(4);
  ```

- **Return** – Permite devolver un dato como el resultado de una funcion. Una peculiaridad de esta keyword es que cualquier código que escribas debajo de el, no se ejecutara.

  Caso **01**

  ```javascript
  function elevarAlCuadrado(numero) {
    return numero * numero; // Si no se añade «return» la funcion devolverá «undefined»
    console.log("Soy un mensaje");
  }

  console.log(elevarAlCuadrado(5));
  ```

  Caso **02**

  ```javascript
  function checkEdad(one) {
    if (one >= 18) {
      console.log("Guao, eres MAYOR de edad");
      return;
    }
    console.log("Aun, eres MENOR de edad");
  }

  checkEdad(18);
  ```

  Caso **03**

  ```javascript
  function mostrarDatos(one, two, three) {
    return `Mi nombre es ${one} ${two}
        y tengo ${three} años.`;
  }

  const red = mostrarDatos("Carlos", "Jose", 25);
  console.log(red);
  ```

### Funcion expresada

Una funcion es un tipo de dato compuesto. Por lo tanto, pueden ser almacenadas dentro de una variable.

Caso **01**

```javascript
comst showMensaje = function (msj) {
  console.log(msj);
};

showMensaje("Hola mundo"); // Ejecutando la funcion
console.log(showMensaje); // Mostrando su código fuente

let showNota = showMensaje; // Copiando la funcion en una variable
showNota(539);
```

Caso **02**

```javascript
function calcular(one, two, operacion) {
  console.log(operacion(one, two));
}

const sumar = function (a, b) {
  // Funcion A
  return a + b;
};

const restar = function (a, b) {
  // Funcion B
  return a - b;
};

calcular(2, 3, sumar);
calcular(2, 3, restar);
```

Caso **03**

```javascript
function askAlgo(pregunta, yes, no) {
  if (confirm(pregunta)) {
    // true OR false.
    yes(); // esto es: "yes = function() {...}", pero para ejecutar el codigo debe ser: "yes()"
  } else {
    no();
  }
}

askAlgo(
  "¿Estás de acuerdo?",
  function () {
    console.log("Claro...");
  }, // Este parámetro se almacena en "yes"

  function () {
    console.log("Olvidado...");
  }
);
```

Por lo general, estas funciones no reciben un nombre `function (...)` a esto se le conoce como _funcion anónima_.

```javascript
const algo = function () {
  //...
};
```

Sin embargo, las funciones anonimas puede ser identificada con un nombre `function nombre(...)` lo que es útil para utilizarla dentro de su codigo (recursividad) o para facilitar la lectura del código.

```javascript
const contador = function accion(numero) {
  // [1]
  console.log(numero);
  const newNumero = numero - 1;

  if (numero > 0) {
    accion(newNumero); // [1] Reutilizando la funcion
  } else {
    console.log("¡Listo!");
  }
};

contador(4);
```

## Funcion flecha

También conocida como _Arrow function_, es una versión compacta de una expresión `function ()`. Se caracteriza por:

- No tener vinculaciones propias cuando se usa `this`, `super`, `arguments` o `new.target`.
- No poder ser usada como constructores.
- _Siempre son anónimas_.
- Entre otras limitaciones.

```javascript
// Antes
let sumar_A = function (a, b) {
  return a + b;
};

console.log(sumar_A(1, 4));

// Ahora
let sumar_B = (a, b) => a + b;

console.log(sumar_B(1, 4));
```

<hr id="line">
