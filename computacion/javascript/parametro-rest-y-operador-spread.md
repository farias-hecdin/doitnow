---

# Javascript, Parámetros REST y operador SPREAD

> Volver a: [[area_informatica]]

```toc

```

<hr id="line_H">

## Parámetros REST

Permite pasar un numero arbitrario de argumentos a una funcion. Para que esto funcione, el ultimo argumento debe llevar el prefijo `...`, es decir:

Ejemplo **01**

```javascript
const sumar = function(...nros) {
  let resultado = 0;

  nros.forEach((valor) => {
    // «X.forEach» – Recorre el Array y pasa una funcion a cada elemento.
    resultado += valor;
  });

  return resultado;
}

console.log(sumar(2, 2, 2, 2, 2)); // 10.
```

Literalmente REST _reune todos los parámetros restantes en un Array_.

## Operador SPREAD

Permite pasar una lista de datos (Array) a una funcion que normalmente requieren una lista de muchos argumentos.

```js
function sum(x, y, z) {
  return x + y + z;
}

const nros = [1, 2, 3, 4, 5]; // Solo se operaran (1 + 2 + 3), ya que la funcion admite solo 3 argumentos.

console.log(sum(...nros)); // 6.
```

El operador SPREAD permite unir Arrays facilmente. Esto evita tener que usar metodos como `.concat(..)`, `.push(..)`, `splice(..)`, entre otros.

```js
let arr1 = [0, 1, 2];
let arr2 = ["A", "B", "C"];
const arr3 = [3, 4, 5];

arr1 = [...arr1, ...arr2, ...arr3]; // arr1 es ahora [0, 1, 2, A, B, C, 3, 4, 5].
console.log(arr1);
```
