# Javascript, Operadores

> Volver a: [inicio](../area-informatica.md)

## Oper. aritméticos

1. Suma: `a + b`
2. Resta: `a - b`
3. Multiplicación: `a * b`
4. División: `a / b`
5. Resto: `a % b`
6. Exponencial: `a ** b`

```javascript
// Declaración de variables
let num1 = 10;
let num2 = 5;

// Operaciones aritméticas
let suma = num1 + num2;
console.log("El resultado es: " + suma) // 15
```

## Oper. de asignación

1. Adicción: `a += 5` es igual a `a = a + 5`
2. Subtracción: `a -= 5` es igual a `a = a - 5`
3. Multiplicación: `a *= 5` es igual a `a = a * 5`
4. y más...

```javascript
// Declaración de variables
let num1 = 10;
let num2 = 5;

// Operador de asignación
num1 += num2;

// Impresión del resultado
console.log("El valor de num1 es: " + num1); // 15
```

### Oper. unario

1. **Pre** – Devuelve inmediatamente la variable operada. Esta puede ser "pre-incrementada" `++x` o "pre-decrementada" `--x`.

```javascript
let num = 5;

console.log(--num); // 4
console.log(num); // 4
```

2. **Pos** – Devuelve primero el valor original y luego la variable será operada. Este es _el operador unario mas comun._

```javascript
let num = 5;

console.log(num--); // 5
console.log(num); // 4
```

## Oper. lógicos

1. OR: `||` – Si al menos una de las condiciones es verdadera, entonces el operador "OR" validará.

```javascript
console.log(true || false); // true
```

1. AND: `&&` – Si ambas condiciones son verdaderas, entonces el operador "AND" validará.

```javascript
console.log(true && false); // false
```

1. NOT: `!` – El símbolo de negación significa que una afirmación verdadera se convierte en falsa, y una afirmación falsa se convierte en verdadera.

```javascript
console.log(!true); // false
```

1. Nullish: `??` – Este simbolo es un operador especial que devuelve el primer valor _definido_.

```javascript
let nombre = null;
let apellido = null;
let sobrenombre = "Pancho";

console.log(nombre ?? apellido ?? sobrenombre ?? "Anónimo"); // "Pancho"
```

## Oper. relacionales

1. Mayor / menor que – `a > b`
2. Mayor / menor igual que – `a >= b`
3. Igual valores – `a == b`
4. No igual valores – `a != b`
5. Valor igual y mismo tipo – `a === b`
6. Valor distinto y diferente tipo – `a !=== b`

```javascript
// Declaración de variables
let edad = 25;
let edadMinima = 18;

// Operador relacional
let esMayorDeEdad = edad >= edadMinima;

// Impresión del resultado
console.log("¿La persona es mayor de edad? " + esMayorDeEdad);
```

Para comparar la igualdad de los valores `a == b` _a partir de ES5 se recomienda usar_ `a === b`.
