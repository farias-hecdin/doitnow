---

# Javascript, Objetos

> Volver a: [[area_informatica]]

```toc

```

<hr id="line_H">

## Objetos literales

Un objeto es una colección de datos y entidades complejas asociadas a un nombre y una clave.

```js
let usuario = new Object(); // Sintaxis de "Constructor de objeto"
let usuario = {}; // Sintaxis de "Objeto literal"
```

Ejemplo **01**

```js
const usuario = {
  // forma de declarar un OBJETO
  name: "John", // la CLAVE "name" almacena el VALOR "John".
  "last name": "Reeves", // "..." para nombrar claves multi-palabras
  age: 32, // la clave mas el VALOR se le llama PROPIEDAD
};

console.log(usuario.name);
```

### Características

- **Añadir** y **sobreescribir** una propiedad de un objeto.

  ```js
  const estudiante = {
    nombre: "Tomas",
    edad: 18,
  };

  // Es posible añadir cualquier tipo de dato (String, Number, Function, etc.)
  estudiante.edad = 22; // Sobreescribiendo una propiedad
  estudiante.sexo = "Hombre"; // Añadiendo una nueva propiedad

  for (let [key, value] of Object.entries(estudiante)) {
    console.log(key, value);
  }
  ```

- **Borrar** una propiedad de un objeto

  ```js
  const estudiante = {
    nombre: "Tomas",
    edad: 18,
  };

  delete estudiante.sexo; // Eliminar una propiedad

  for (let [key, value] of Object.entries(estudiante)) {
    console.log(key, value);
  }
  ```

### Acceder a un objeto

Caso **01**

```js
const estudiante = {
  nombre: "Tomas",
  edad: 18,
};

console.log(estudiante.nombre);
```

Caso **02**

```js
// Accediendo al valor de un objeto anidado

const estudiante = {
  nombre: "Tomas",
  edad: 18,
  notas: {
    ciencia: 17,
    algebra: 15,
    ingles: 18,
  },
};

console.log(estudiante.notas.algebra);
```

Caso **03**

```js
// Iterando una serie de objetos

const listaDeDatos = [
  { id: "001", name: "Diego", age: 32, company: "TESLA Inc." },
  { id: "002", name: "Ana", age: 25, company: "APPLE Inc." },
  { id: "003", name: "Eric", age: 36, company: "MICROSFT Inc." },
];

function iterar(datos) {
  let elemento, clave;
  clave = prompt("¿Que te gustaria saber?", "name");

  for (let i = 0; i < datos.length; i++) {
    elemento = datos[i][clave];

    alert(elemento);
  }
}

iterar(listaDeDatos);
```

### Métodos

Un método es básicamente una propiedad de un objeto que posee una función como un valor.

```js
const persona = {
  nombre: "Tovy",
  nacionalidad: "Española",
  saludo: function () {
    console.log("Que pasa tio");
  },
};

persona.saludo(); // "Que pasa tio"
```

- Sintaxis _regular_.

  ```js
  const pais = {
    nombre: "Suiza",
    codigo: function () {
      // creando un METODO
      console.log("CHE");
    },
  };

  pais.codigo(); // Ejecutando el metodo
  ```

- Sintaxis _simplificada_.

  ```js
  const pais = {
    nombre: "Suiza",
    codigo() {
      // creando un METODO
      console.log("CHE");
    },
  };

  pais.codigo(); // Ejecutando el metodo
  ```

### this en Javascript

`this` es una expresion que te permitira acceder a una propiedad determina de un objeto desde un _método_ dentro del mismo objeto.

```js
const persona = {
  firstName: "Martin",
  age: 30,
  saludo: function () {
    const lastName = "Diaz";
    console.log(`Hola, soy ${this.firstName} ${lastName}`);
    console.log(this);
  },
};

persona.saludo();
```

Dado a que la expresion `this` presenta muchas particularidades en javascript, esta seran cubiertas en otro articulo llamado _this_.

## Constructor

- **Función constructora**

  Caso **01**

  ```js
  // [1] se crea el objeto
  function Persona(name, age) {
    // la funcion se escribe en mayuscula por convencion
    this.name = name;
    this.age = age; // Aca «this» hace referencia al objeto.
    this.isAdmin = false;
    this.hello = function () {
      return "¡Que onda!";
    };
  }

  // [2] se añaden las propiedades
  const sujetoUno = new Persona("Miguel", 23);

  console.log(sujetoUno.name);
  console.log(sujetoUno.hello());
  ```

  Caso **02**

  ```js
  function Persona(personName, personAge, personGender) {
    this.name = personName;
    this.age = personAge;
    this.gender = personGender;
    this.greet = function () {
      return `Hello, ${this.name}`;
    };
  }

  const sujeto1 = new Persona("John", 23, "male");
  const sujeto2 = new Persona("Sam", 25, "female");

  console.log(sujeto1.name);
  console.log(sujeto2.name);
  ```

- **Clase**

  ```js
  // Funcion constructora
  function PerroFn(nombre, color) {
    this.nombre = nombre;
    this.color = color;
    this.ruido = () => `Guao.. Guao`;
  }

  const dobermam = new PerroFn("luis", "negro");
  console.log(dobermam.ruido());

  // Clase
  class PerroCl {
    constructor(nombre, color) {
      this.nombre = nombre;
      this.color = color;
    }
    ruido = () => `Guao.. Guao`;
  }

  const pitbull = new PerroCl("Diego", "azul");
  console.log(pitbull.ruido());
  ```

<hr id="line_H">

[[#Javascript Objetos|Inicio]]
