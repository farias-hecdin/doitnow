## Bucles

### For

```javascript
/* 1. Se lee la expresión.
   2. Luego se evalúa si se cumple con la condición.
   3. Si es asi, se ejecuta el bloque de código.
   4. Y por ultimo, se actualiza la expresión. */

// Este proceso se repetirá hasta que la condición ya no se cumpla.

// for («[1] inicializacion»; «[2] condición»; «[4] incremento») {
//     «[3] bloque de código»
// }

/* Ejemplo: */

let name = prompt("¿Como te llamas?", "Anonimo")
let num = 5;

for (let i = 0;  i <= num;  i++) {
  // El ciclo se ejecutará hasta qué (i = 5)
  console.log(`Hola ${name}`);
}

console.log("Listo!")
```

Como nota adicional, todas las expresiones dentro del paréntesis son opcionales.

```javascript
let arreglo = [1, 2, 3, 4];

for (let i = 0; arreglo.length < 8; ) {
    console.log(`Tamaño: ${arreglo.length}`, `Array: ${arreglo}`);
    arreglo.push(arreglo[i]);
    i++;
}

console.log(arreglo);
```

#### For..in.. y For..of..

- `for..in..` – Crea un bucle que itera sobre las propiedades enumerables (keys) de un objeto. Por lo general, esta expresión _no se recomienda usar en Arrays_, ya que no se puede garantizar que la iteración ocurra en un orden especifico. En su lugar, es mejor utilizar `for..` o `for..of..`.

```javascript
const dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
const animales = {
  ave: "Loro",
  mamifero: "Perro",
  reptil: "Iguana",
  pez: "Tiburon",
};

// En Array: Es valido usarlo, pero ¡No es recomendado!
for (let item in dias) {
  console.log(item);
}

// En Object.
for (let item in animales) {
  console.log(item);
}
```

- `for..of..` – Crea un bucle que itera sobre cualquier objeto iterable (String, Arrays, Object, etc).

```javascript
const dias = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
const animales = {
  ave: "Loro",
  mamifero: "Perro",
  reptil: "Iguana",
  pez: "Tiburon",
};

// En Array.
for (let item of dias) {
  console.log(item);
}

// En Object: Con la ayuda del metodo Object.entries(..)
for (let [itemClave, itemValor] of Object.entries(animales)) {
  console.log(itemClave, itemValor);
}
```

### While..

La sintaxis de la estrctura `While..` funciona de la siguiente manera:

1. Se evalúa la condición.
2. Si se cumple, se ejecuta el bloque de código.

Este proceso se repetirá hasta que la condición ya no se cumpla.

Caso **01**

```javascript
let i = 0;

while (i < 3) {
  console.log(i);
  i++;
}
```

### Do..while..

La sintaxis de la estrctura `do..while..` es similar al bucle `while..`. Su sintaxis es la siguiente:

1. Se ejecuta el código primero.
2. Luego la condición es evaluada.
3. Si se cumple la condición se ejecuta el bloque de código nuevamente.

Este proceso se repetirá hasta que la condición ya no se cumpla.

```javascript
let i = 0;

do {
  console.log(i);
  i++;
} while (i < 3);
```

### ¿Como salir de un bucle o una estrctura de control

- `break` – Se utiliza para salir de un bucle o una de una estrctura de control.

```javascript
const numeros = [1, 4, -3, 5, 2];

let i = 0;

while (i < numeros.length) {
  if (numeros[i] < 0) {
    break;
  }
  console.log(numeros[i]);
  i++;
}
```

- `continue` – Se utilizar dentro de un bucle para saltar a la siguiente iteracion sin ejecutar el resto del codigo de la iteracion actual.

```javascript
const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (let i = 0; i < numeros.length; i++) {
  if (numeros[i] % 2 !== 0) {
    continue;
  }
  console.log(numeros[i]);
}
```

- `(label)` – Es una etiqueta que permite salir de multiples bucles anidados que ha sido detenido por la expresion `break`.

Caso __01__

```javascript
let numero = 10;

// Puedes usar "outer" o cualquier otra palabra como identificador
outer: for (let i = 0; i < numero; i++) {
  if (i === 5) {
    break;
  }
  console.log(`iteracion ${i} de ${numero}`);
}

console.log("Fin del bucle");
```

Caso **02**

```javascript
const formFields = [
  { name: 'nombre', value: 'Juan', required: true },
  { name: 'apellido', value: '', required: true },
  { name: 'edad', value: '35', required: false },
  { name: 'email', value: 'juan@example.com', required: true },
  { name: 'telefono', value: '123456789', required: false },
];

let isValid = true;

outer: for (let i = 0; i < formFields.length; i++) {
  const field = formFields[i];
  if (field.required && !field.value) {
    alert(`El campo ${field.name} es obligatorio`);
    isValid = false;
    break outer;
  }
  if (field.name === 'edad' && isNaN(field.value)) {
    alert(`El campo ${field.name} debe ser un número`);
    isValid = false;
    break outer;
  }
  // Lógica de validación adicional para otros campos...
}

if (isValid) {
  // enviar datos del formulario
}
```
