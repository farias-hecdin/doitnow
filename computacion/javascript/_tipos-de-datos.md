# Javascript, Tipos de datos

> Volver al [Inicio](../area-informatica.md)

## Tipos de datos

El estándar ECMAScript define 9 tipos de datos que se clasifican en dos grupos llamados primitivos y compuestos.

1. Datos primitivos
   - Number - `123` ; `1_000_000` ; `3.145`
   - BigInt - `12345678901234567890n`
   - String - `'Hello world...'` ; `"Hello world..."` ; `´Hello ${name} world...´`
   - Boolean - `true` ; `false`
   - Symbol - `Symbol("Helo world")`
   - Undefined - `let name`
   - Null - `let name = null`
2. Datos compuestos
   - Object - `{..}`
     - Array - `[..]`
     - Class - `class name {..}`
     - etc...
   - Function - `function name() {..}` ; `() => {..}`

Los datos primitivos (inmutables) no se pueden cambiar después de ser creados, pero sí se puede reasignar la variable con un nuevo valor.

Los datos compuestos (mutables) pueden ser cambiados después de ser creados y tienen métodos para manipular su contenido.

Caso **01**

```javascript
// Primitivos
let animals = "Elephann";
// Oh, esta mal escrito. Intentemos corregir el error.

animals[7] = "t";
console.log(animals); // El error no puede ser solucionado.
```

Caso **02**

```javascript
// Compuesto
const animals = ["kkat", "own", "dog"];
// Oh, esta mal escrito. Intentemos corregir el error.

animals[0] = "cat";
console.log(animals); // Error solucionado.
```

A través de la función `typeof(..)` podemos descubrir _¿Qué tipo de dato es?_ un dato asignado.

```javascript
let one = 123;
let two = "January";

console.log(typeof(one)); // El dato es del tipo: Number.
console.log(typeof(two)); // El dato es del tipo: String.
```

## Datos primitivos

### Number

Representa valores numéricos como enteros (24), flotantes (3.141) o numeros negativos (-121.5).

```javascript
let var_one = new Number(6500); // Declaración formal.
let var_two = 402; // Declaración convencional.

console.log(var_one);
console.log(var_two);
```

```javascript
let var_one = 25; // enteros.
let var_two = 3.1415; // flotantes.

console.log(var_one, var_two);

// EXTRA: Formas validas de escribir cifras grandes (ej. millones).
let var_three = 1000000; // Normal.
let var_four = 1_000_000; // Mas legible.
let var_five = 1e6; // Mas corto.

console.log(var_three, var_four, var_five);
```

#### Métodos

##### X.toFixed(..)

`.toFixed(«Decimales»)` – Es utilizado para aproximar el valor decimal de un número y retornarlo como un String. Este es el método más confiable para redondear números.

```javascript
let var_one = 5.687;

console.log(+varone.tofixed(2)); // 5.68 (el signo "+" transforma el dato a Number).
```

##### parseInt(..) / parseFloat(..)

`parseInt(«Valor»)` y `parseFloat(«Valor»)` – Es utilizado para casting, es decir, para extraer los valores numéricos de un string y convertirlos en números enteros o flotante respectivamente. Esto es muy útil para obtener el valor de una unidad.

```javascript
let var_one = "3.25mm";
let var_two = "$43";

// Se extrae la parte numérica solo si el String comienza con números.
console.log(parseFloat(var_one)); // 3.25
console.log(parseFloat(var_two)); // NaN
```

##### isNaN(..) / isInteger(..)

`Number.isNaN(«Valor»)` – Es utilizado para determina si el valor dado _no es un número_, de ahí sus nombre isNaN (es decir: is Not a Number), mientras que `Number.isInteger(«Valor»)` – Determina si el valor _es un entero_.

```javascript
let var_one = 3.1415;
let var_two = "Devid";

console.log(isNaN(var_one)); // false
console.log(isNaN(var_two)); // true
console.log(isInteger(var_one)); // false
```

#### Más métodos útiles

JavaScript tiene incorporado una pequeña biblioteca de metodos para calculos matemáticos y otras operaciones.

- `Math.random()` – Devuelve números aleatorios comprendidos entre 1 y 0. La función a continuacion devolverá números aleatorios de acuerdo a un intervalo máximo y mínimo.

  ```javascript
  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  getRandomArbitrary(500, 999);
  ```

- `Math.round(«Valor»)` – Es utilizado para redondear un numero dado a su entero más cercano.

Para más métodos, consulte el [manual](https://tinyurl.com/4222bfjp).

### String

Es una cadena de caracteres que se utiliza para representar un texto.

```javascript
// Formas de escribir un String.
let var_one = "Este es un texto de una linea"; // Comillas dobles.
let var_two = 'Este es un texto de una linea'; // Comillas simples.
let var_three = `Este es un texto
de multiples lineas con una
etiqueta de apertura y una de cierre.`; // Template String.

// Al usar «Template string» podemos añadir saltos de línea e insertar variables dentro del String.
let name = "Maria",
let message = `Hola, soy un ${name} y tengo ${20 + 12 - 10} años de edad`;

console.log(message);
```

A partir de la versión ES5 de Javascript, es posible manipular un String como si fuera un Array. Esto significa que podemos recorrer un String y utilizar algunos de los métodos que se utilizan en los Arrays.

```javascript
// Recorriendo un String.
let var_one = "girasol";

// Por paso.
console.log(var_one[2]); // "r"

// Por iteración.
let texto_iterado;

for (texto_iterado of var_one) {
  console.log(texto_iterado); // "g,i,r,a,s,o,l".
}
```

#### Métodos

##### X.toUpperCase() / X.toLowerCase()

`.toUpperCase()` devuelve el texto en mayúscula, mientras que `.toLowerCase()` devuelve el texto en minúscula.

```javascript
let var_one = "aeiou";

console.log(var_one.toUpperCase());
console.log(var_one[1].toUpperCase());
```

##### X.includes(...)

`.includes(«Valor», «Posicion»)` – Devuelve un booleano si el String existe.

```javascript
let var_one = "Murcielago";
let var_two = "Caballo";

console.log(var_one.includes("ie")); // true.
console.log(var_two.includes("bal", 2)); // true.
```

##### X.replace(...)

`.replace(«Patron», «Texto_a_remplazar»)` – Devuelve un nuevo String que concuerde con todas las coincidencias del patrón, ya sea un String o un RegExp.

```javascript
let old_text = "Mr blue has a blue house and a blue car.";
let pattern = "red";

let new_text = old_text.replace(pattern, "blue"); // Para reeplazar todas las coincidencia debes usar RegExp

console.log(new_text);
```

##### X.slice(...)

`.slice(«Comienzo», «Final»)` – Devuelve una subcadena de un String, la cual se extiende desde el índice especificado.

```javascript
let var_one = "ABRELATAS";

console.log(var_one.slice(0, 3)); // "ABR", no se incluye la posicion final.
console.log(var_one.slice(4)); // "LATAS".
```

#### Más métodos String útiles

- `X.trim()` – Remueve cualquier espacios en blanco al comienzo y al final de un String.

  ```javascript
  let var_one = "  Javascript no es Java   ";

  console.log(var_one.trim());
  ```

- `X.repeat(«Veces»)` – Repite un String una n cantidad de veces.
- `X.match(«Patron»)` – Realiza una busqueda y devuelve un Array.

  ```javascript
  let text = "Find great tutorials at https://www.lenguajejs.com/functions/";

  let pattern = /(\w+):\/\/([\w.]+)\/(\S*)/;
  let coincidence = texto.match(pattern);

  console.log(coincidence);
  console.log(coincidence[0]);
  console.log(coincidence[1]);
  ```

* `X.split(«Separador»)` – Convierte un String en un Array, separadas por un caracter dado.
* `X.charAt(«Posicion»)` – Retorna el caracter en la posicion X del String.
* `X.concat(«String_1», «String_2» ...)` – Conbina uno o mas String y devuelve un nuevo String.

Para más métodos consulta el [manual](https://tinyurl.com/4w387brt).

## Boolean

Los booleanos son un tipo de dato lógico que puede devolver solo los valores `true` o `false`. No obstante, estos valores puede ser representados de varias formas, como por ejemplo:

```javascript
let var_one = true // true
let var_two = false // false
let var_three = 123 // true
let var_four = 0 // false
let var_five = "texto" // true
let var_six = "" // false
```

## Null / Undefined

- `undefined` – Es un valor vacío _asignado por Javascript_ cuando detecta que la variable no se le ha asignado algun valor.

  ```javascript
  let red;

  console.log(red); // undefined.
  ```

- `null` – Es un valor vacío _asignado por el programador_. Esto es útil cuando queremos crear una variable vacia para añador luego algun valor.

  ```javascript
  let red = null;

  console.log(red); // null.
  ```
