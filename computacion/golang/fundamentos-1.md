# Fundamentos 1

> [Regresar](../zona-de-inicio.md)

# Comentarios

```go
// Este es un comentario de una sola linea.

/* Este es un comentario de multiples lineas
   con una etiqueta de apertura y una
   de cierre. */
```

## Hello world

```go
/* Todos los proyecto en Go deben tener un archivo "main.go" que contenga las
   lineas (1) y (2) */

package main // (1)

import (
    "fmt"
)

func main() { // (2)
    fmt.Println("Hello world")
}
```

## Variables y tipos de variables

```go
var numero int = 365
```

### Formas de declarar una o mas variable

```go
 // Forma No.1
var edad int = 32
var nombre, apellido int = "Juan", "Perez"

 // Forma No.2
var año = 2024
var mes, dia = "Enero", "Lunes"

 // Forma No.3
horas := 24
minutos, segundos := 60, 60
```

## Interacciones

```go
// Usando el paquete "fmt"
fmt.Println()
fmt.Print()
```

## Tipos de datos

### Primitivos

```go
/* String */
var nombre string = "Diego"
/* Numero */
var dias int = 365
var gravedad = 9.81
```
