# Fundamentos 1

> [Regresar](../zona-de-inicio.md)

## Comentarios

```php
<?php
// Este es un comentario de una sola linea.

# Este es otro comentario de una sola linea.

/* Este es un comentario de multiples lineas
   con una etiqueta de apertura y una
   de cierre. */
```

## Variables y tipos de variables

```php
<?php
# $ - Permite declarar variables.
$nombre = 'Diego';

# const - Perimite declarar constantes.
const APELLIDO = "Samg";
# or
define('APELLIDO', 'Samg')
```

### Formas de declarar una o mas variable

```php
<?php
// Forma No.1

```

## Interacciones

```php
<?php
echo "Hello world"

print("Hello world with print")

print_r("Hello world with print_r")
```

## Tipos de datos

```php
<?php
# Primitivos
$texto = 'Hola mundo'; // string.
$numero = 123; // number
$boleano = true; // boolean

# Compuestos
$colores = ['red', 'blue', 'gray']; // Array
$pais = [
    'VEN' => 'Venezuela',
    'USA' => 'EE.UU'
];

# Especiales
$nulo = null;
```

## Condicionales

### If / else

```php
<?php
/* if.. else.. - Esta expresion sigue el patron: Si la condición se cumple,
   ejecuta la accion indicada. Sino se cumple, ejecuta esta otra. */

$edad = 19;

if (edad > 19) {
    echo "Tienes {$edad}. Eres mayor de edad.";
} else {
    echo "Eres menor de edad."
}
```
