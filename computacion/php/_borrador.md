# Php

## Como ejecutar un programa en Php
Al igual que JavaScript, los programas de PHP tienen la capacidad de ejecutarse en el navegador. Sin embargo, su ejecucion no es tan sencilla, ya que requiere configurar antes nuestro entorno de desarrollo. No obstante, dado que nuestro enfoque principal es aprender los fundamentos de PHP, solo necesitaremos crear un archivo con extensión `.php` y utilizar el comando `php` seguido del archivo a ejecutar desde la terminal.

```php
<?php
# Creando un simple programa que devuelve el mensaje Hello world.
$message = 'Hello world';

echo $message
```

```bash
php "script.php"
```

## Comentarios
Existen tres manera de realizar un comentario en PHP.

* Comentarios de una sola linea: `//` o `#`
```php
<?php
# Este es un comentario de una linea.

// Este es un comentario de una linea.
```

* Comentarios de bloque o multiples lineas: `/* */`
```php
<?php
/* Este es un comentario de multiples
lineas con una etiqueta de apertura
y una de cierre.
*/
```

## Variables
Para declarar una variable en PHP utilizamos el símbolo `$` seguido del nombre de la variable.

```php
<?php
$nombre = "Juan";
$edad = 25;
$suma = 10 + 15;
$_variable_privada = 'PRIVADO'
```

Las variables solo pueden contener letra, numeros o `_`. No pueden contener espacios o palabras reservadas como `true` o `return`.

```php
<?php
1nombre // No puede empezar con un número
$ nombre // No puede contener espacios
$#variable // No puede contener caracteres especiales
```

En Php, podemos usar la sintaxis de asignación múltiple para asignar el mismo valor a varias variables.

```php
<?php
$host = $port = $database = $username = $password = null;
```

### Constantes
Las constantes son variables que no pueden ser cambiadas una vez que han sido definidas. Existen dos formas de declararlas:

```php
<?php
const NAME = 'diego';
```

```php
<?php
define('AGE', 23);
```

## Tipos de datos

Los tipos de datos más comunes en PHP son:

```php
<?php
$entero = 1000;
$flotante = 9.81;
$exponencial = 981e-2;

$texto = 'Hola mundo';

$booleano = true

$array = ['a', 'b', 'c']
$array2 = array(
    'word1' => 'a',
    'word2' => 'b',
)

$nulo = NULL
```

Con la expresion `gettype()` puedes descubrir el tipo de dato de una variable.

```php
<?php
$variable = 1234;

echo gettype(variable); // Integer
```

## Oper. aritmeticos, oper. comparadores y oper. logicos
Se escribe igual que en javascript.

```php
```
## Condicionales y bucles

### Condicionales

- `if..` – Esta expresion sigue el patron: Si la condición se cumple, ejecuta la acción indicada; si no se cumple, pasa de el.

```php
<?php
$day = date("D");

if($day == "Fri"){
    echo "Have a nice weekend!";
}
```

- `if.. else..` – Esta expresion sigue el patron: Si la condición se cumple, ejecuta la accion indicada. Sino se cumple, ejecuta esta otra.

```php
<?php
$day = date("D");

if($day == "Fri"){
    echo "Have a nice weekend!";
} elseif($d == "Sun"){
    echo "Have a nice Sunday!";
} else{
    echo "Have a nice day!";
}
```

### Ternario y operador nullish

`?` – Esta es una expresión que simplifica la forma de escribir condicionales `if.. else..`.

```php
<?php
echo ($age < 18) ? 'Child' : 'Adult';
```

`??` – Este es un operador lógico que retorna el operando de lado derecho cuando el operando de lado izquierdo es indefinido.

```php
<?php
$name = $_GET['name'] ?? 'anonymous';

echo "Hola, $name!";
```

### Switch

```php
<?php
$n = 5;

switch ($n) {
    case 1:
        echo "El valor de n es 1.";
        break;
    case 2:
        echo "El valor de n es 2.";
        break;
    case 3:
        echo "El valor de n es 3.";
        break;
    default:
        echo "El valor de n es diferente de 1, 2 y 3.";
        break;
}
```

## Array
Hay 3 tipos de array que podemos crear en Php

* Indexedo — An array with a numeric key.
* Associativo — An array where each key has its own specific value.
* Multidimensional — An array containing one or more arrays within itself.

```php
<?php
// Define an indexed array
$colors = array("Red", "Green", "Blue");
// or
$numeros = ['Uno', 'Dos', 'Tres'];

echo $colors[0]
```

```php
<?php
// Define an associative array
$ages = array("Peter"=>22, "Clark"=>32, "John"=>28);
```
